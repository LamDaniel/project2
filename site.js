//Daniel Lam (1932789) and Grigor Mihaylov (1937997)
"use strict"; //always

//invoke setup() function once pictures and DOM have been loaded.
window.addEventListener('load', setup);
let global = {}; //namespace

/**
 * setup() declares all the global variables and adds an event listener on the button to change the state.
 * Everytime the input gets changed, it sets up the local stoage and stores the value of WPM from input.
 * It also loads the JSON from your local storage (the wpm value).
 * @author Daniel Lam
 * @author Grigor Mihaylov
 */
function setup() {
    global.stopState = true;
    global.wpm = document.querySelector("#wpm");
    global.btn = document.querySelector("button");
    global.spanBefore = document.querySelector('#before');
    global.spanFocused = document.querySelector('#focused');
    global.spanAfter = document.querySelector('#after');

    global.btn.addEventListener("click", changeState);
    global.wpm.addEventListener("change", () => {
        global.speed=global.wpm.value;
        let jsonWPM = JSON.stringify(global.wpm.value); //sets up local storage
        localStorage.setItem("WPM", jsonWPM);
    });

    //Loads local storage
    let jsonWPM = localStorage.getItem("WPM");
    if (jsonWPM) {
        global.wpm.value = JSON.parse(jsonWPM);
    }
    else {
        global.wpm.value = 100;
    }
}

/**
 * changeState() changes the state of the program.
 * If it's on stop, change to "Start" and invokes getNextQuote() to fetch a quote from the API.
 * If it's on start, change to "Stop" and clears the quote interval as well as remove the quote.
 * @author Daniel Lam
 * @author Grigor Mihaylov
 */
function changeState() {
    if (global.stopState) {
        global.btn.innerText = "Stop";
        global.stopState = false;

        global.speed=global.wpm.value;
        getNextQuote();
    } else {
        global.btn.innerText = "Start";
        global.stopState = true;
        clearInterval(global.interval);

        global.spanBefore.innerText = '';
        global.spanFocused.innerText = '';
        global.spanAfter.innerText = '';
    }
}

/**
 * getNextQuote() sends a response to the Ron Swanson API and returns a JSON if it returns status code 200.
 * It splits the quote with every space into an array and invokes displayQuote using the split array as its parameters.
 * If there is any error, print error message into console.
 * @author Daniel Lam
 * @author Grigor Mihaylov
 */
function getNextQuote() {
    fetch('https://ron-swanson-quotes.herokuapp.com/v2/quotes') //No rights reserved.
    .then(response => {
        if (!response.ok) {
            throw new Error('Error code:' + response.status);
	    }
        return response.json();
    })
    .then(json => json[0].split(' '))
    .then(words => displayQuote(words))
    .catch(error => console.error("Couldn't fetch the quote : " + error));
}

/**
 * displayQuote() takes in an array of strings and invokes a variable to store the index at 0.
 * It also sets up the interval at the speed of 60000/wpm and interval invokes displayWord() with words array as its parameters.
 * @param {*} words array of split words from quote
 * @author Daniel Lam
 * @author Grigor Mihaylov
 */
function displayQuote(words) {
    global.index = 0;
    global.interval = setInterval(displayWord, 60000/global.speed, words);
}

/**
 * displayWord() takes in an array of words, prints it out onto the screen and focuses the letter in red based on the length of the word.
 * Once it reaches the end of the array, it clears the interval of the current quote.
 * If it's still in start state, it invokes getNextQuote() and fetches the the next quote to display on screen.
 * @param {*} words 
 * @author Daniel Lam
 * @author Grigor Mihaylov
 */
function displayWord(words) {
    let newWord = words[global.index];
    if (newWord.length == 1) {
        global.spanBefore.innerText = "    ";
        global.spanFocused.innerText = newWord;
        global.spanAfter.innerText = '';
    }
    else if (newWord.length <= 5) {
        global.spanBefore.innerText = "   " + newWord.charAt(0);
        global.spanFocused.innerText = newWord.charAt(1);
        global.spanAfter.innerText = newWord.substring(2, newWord.length);
    }
    else if (newWord.length <= 9) {
        global.spanBefore.innerText = "  " + newWord.substring(0, 2);
        global.spanFocused.innerText = newWord.charAt(2);
        global.spanAfter.innerText = newWord.substring(3, newWord.length);
    }
    else if (newWord.length <= 13) {
        global.spanBefore.innerText = " " + newWord.substring(0, 3);
        global.spanFocused.innerText = newWord.charAt(3);
        global.spanAfter.innerText = newWord.substring(4, newWord.length);
    }
    else {
        global.spanBefore.innerText = newWord.substring(0, 4);
        global.spanFocused.innerText = newWord.charAt(4);
        global.spanAfter.innerText = newWord.substring(5, newWord.length);
    }
    global.index++;

    //Once it reaches the end the array, clear interval and fetch next quote.
    if (words.length == global.index) {
        clearInterval(global.interval);
        if (!global.stopState) {
            getNextQuote();
        }  
    }
}